{-# LANGUAGE OverloadedStrings #-}

import Wasmjsbridge
import System.Random
import Control.Monad

foreign export ccall hello :: IO ()

hello :: IO ()
hello = do	
	getElementById <- get_js_object_method (JSObjectName "document") "getElementById"
	canvas <- call_js_function_ByteString_Object getElementById "myCanvas"
	getContext <- get_js_object_method canvas "getContext"
	ctx <- call_js_function_ByteString_Object getContext "2d"
	moveTo <- get_js_object_method ctx "moveTo"
	lineTo <- get_js_object_method ctx "lineTo"
	stroke <- get_js_object_method ctx "stroke"
	randlines 400 moveTo lineTo stroke

randlines n moveTo lineTo stroke
	| n > 0 = do
		x1 <- randomX
		y1 <- randomY
		x2 <- randomX
		y2 <- randomY
		call_js_function_Int_Int_Void moveTo x1 y1
		call_js_function_Int_Int_Void lineTo x2 y2
		call_js_function_Void stroke
		randlines (n-1) moveTo lineTo stroke
	| otherwise = return ()

randomX :: IO Int
randomX = randomRIO (0, 600)

randomY :: IO Int
randomY = randomRIO (0, 300)

alertdemo :: IO ()
alertdemo = do
	alert <- get_js_object_method (JSObjectName "window") "alert"
	call_js_function_ByteString_Void alert "hello, world!"

-- Dummy, not used, but needed to compile for unknown reason.
main :: IO ()
main = undefined
