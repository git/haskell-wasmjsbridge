import { WASI } from './browser_wasi_shim.js';
import { Wasmjsbridge } from './wasmjsbridge.js';

async function run() {
    const wasi = new WASI([], [], []);
    const wasmjsbridge = new Wasmjsbridge();
    const importObj = 
		{ wasi_snapshot_preview1: wasi.wasiImport
		, wasmjsbridge: wasmjsbridge.bridgeImport
		};
    const wasm = await WebAssembly.instantiateStreaming(fetch("./Hello.wasm"), importObj);
    wasi.inst = wasm.instance;
    const exports = wasm.instance.exports;
    wasmjsbridge.init(exports, 0, 0);
    exports.hello();
}

run();
