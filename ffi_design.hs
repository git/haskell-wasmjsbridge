-- On the javascript side, need a data structure to store the
-- values pointed to by handles. Eg a javascript Map from the Int
-- in the JSHandle to the javascript value.
newtype JSHandle = JSHandle Int

newtype JSFunctionHandle = JSFunctionHandle JSHandle

-- Looks up the javascript function with the specified name, and
-- returns a handle to it.
-- Note that this needs String passing through the FFI and decoding
-- in Javascript. Ugh.
-- Also this will need to use eval in the javascript implementation. That
-- is slow. Returning a JSFunctionHandle allows caching it, which can be
-- done on the haskell side.
get_js_function :: String -> IO JSFunctionHandle

newtype JSObject = JSObject JSHandle

-- Gets a method of the object.
get_js_method :: JSObject -> String -> IO JSFunctionHandle

-- Get a setter for a variable of an object.
-- Eg, object.name = value
-- (On the javascript side, this can just eval code to make a function that
-- takes a value and runs that, and store the function in the Map.
-- But, what about caching? And when to remove from map?
-- XXX have a toplevel Map in javascript of objects the haskell code is
-- using, and store in each object a Map of functions.
get_js_setter :: JSObject -> String -> IO JSFunctionHandle

-- Ways to call javascript functions of different types.
call_js_function_Int_Int :: JSFunctionHandle -> Int -> IO Int
call_js_function_Int_Void :: JSFunctionHandle -> Int -> IO ()
call_js_function_String_Int :: JSFunctionHandle -> String -> IO Int
call_js_function_String_Void :: JSFunctionHandle -> String -> IO ()
call_js_function_Int_JSObject :: JSFunctionHandle -> Int -> IO JSObject
call_js_function_String_JSObject :: JSFunctionHandle -> String -> IO JSObject
call_js_function_JSHandle_Int :: JSFunctionHandle -> JSHandle -> IO Int
call_js_function_JSHandle_JSObject :: JSFunctionHandle -> JSHandle -> IO JSObject
call_js_function_JSHandle_Void :: JSFunctionHandle -> JSHandle -> IO ()

-- Frees the javascript object referred to by the JSHandle.
free_js_handle :: JSHandle -> IO ()

free_js_object :: JSObject -> IO ()
free_js_object (JSObject h) = free_js_handle h

withElementById :: String -> (JSObject -> IO a) -> IO a
withElementById i = bracket setup free_js_object
  where
	setup = do
		f <- get_js_function "document.getElementById"
		call_js_function_String_JSObject f i

setElementColor :: JSObject -> String -> IO ()
setElementColor o c = do
	f  <- get_js_setter o "style.color"
	call_js_function_String_Void f c

redfoo :: IO ()
redfoo = withElementById "foo" $ \o -> setElementColor o "red"

