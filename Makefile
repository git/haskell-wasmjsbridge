all:
	wasm32-wasi-cabal install --installdir=. --overwrite-policy=always --install-method=copy

clean:
	cabal clean
