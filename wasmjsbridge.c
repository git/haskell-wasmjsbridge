#include <stdint.h>

uint32_t _get_js_objectname_method(uint8_t *obuf, uint32_t olen, uint8_t *fbuf, uint32_t flen) __attribute__((
	__import_module__("wasmjsbridge"),
	__import_name__("get_js_objectname_method")
));

uint32_t get_js_objectname_method(uint8_t *obuf, uint32_t olen, uint8_t *fbuf, uint32_t flen) {
	return _get_js_objectname_method(obuf, olen, fbuf, flen);
}

uint32_t _get_js_objectid_method(uint32_t i, uint8_t *fbuf, uint32_t flen) __attribute__((
	__import_module__("wasmjsbridge"),
	__import_name__("get_js_objectid_method")
));

uint32_t get_js_objectid_method(uint32_t i, uint8_t *fbuf, uint32_t flen) {
	return _get_js_objectid_method(i, fbuf, flen);
}

void _call_js_function_void(uint32_t fn) __attribute__((
	__import_module__("wasmjsbridge"),
	__import_name__("call_js_function_void")
));

void call_js_function_void(uint32_t fn) {
	_call_js_function_void(fn);
}

void _call_js_function_string_void(uint32_t fn, uint8_t *buf, uint32_t len) __attribute__((
	__import_module__("wasmjsbridge"),
	__import_name__("call_js_function_string_void")
));

void call_js_function_string_void(uint32_t fn, uint8_t *buf, uint32_t len) {
	_call_js_function_string_void(fn, buf, len);
}

uint32_t _call_js_function_string_int(uint32_t fn, uint8_t *buf, uint32_t len) __attribute__((
	__import_module__("wasmjsbridge"),
	__import_name__("call_js_function_string_int")
));

uint32_t call_js_function_string_int(uint32_t fn, uint8_t *buf, uint32_t len) {
	return _call_js_function_string_int(fn, buf, len);
}

uint32_t _call_js_function_string_object(uint32_t fn, uint8_t *buf, uint32_t len) __attribute__((
	__import_module__("wasmjsbridge"),
	__import_name__("call_js_function_string_object")
));

uint32_t call_js_function_string_object(uint32_t fn, uint8_t *buf, uint32_t len) {
	return _call_js_function_string_object(fn, buf, len);
}

void _call_js_function_int_int_void(uint32_t fn, uint32_t x, uint32_t y) __attribute__((
	__import_module__("wasmjsbridge"),
	__import_name__("call_js_function_int_int_void")
));

void call_js_function_int_int_void(uint32_t fn, uint32_t x, uint32_t y) {
	_call_js_function_int_int_void(fn, x, y);
}
