module Wasmjsbridge where

import Foreign.C.String
import qualified Data.ByteString as B
import qualified Data.ByteString.Unsafe as BU

foreign import ccall unsafe "get_js_objectname_method"
	_get_js_objectname_method :: CString -> Int -> CString -> Int -> IO Int

foreign import ccall unsafe "get_js_objectid_method"
	_get_js_objectid_method :: Int -> CString -> Int -> IO Int

foreign import ccall unsafe "call_js_function_void"
	_call_js_function_void :: Int -> IO ()

foreign import ccall unsafe "call_js_function_string_void"
	_call_js_function_string_void :: Int -> CString -> Int -> IO ()

foreign import ccall unsafe "call_js_function_string_int"
	_call_js_function_string_int :: Int -> CString -> Int -> IO Int

foreign import ccall unsafe "call_js_function_string_object"
	_call_js_function_string_object :: Int -> CString -> Int -> IO Int

foreign import ccall unsafe "call_js_function_int_int_void"
	_call_js_function_int_int_void :: Int -> Int -> Int -> IO ()

newtype JSFunction = JSFunction Int

-- | A javascript object can be referred to either by name
-- (eg "window") or by reference
data JSObject = JSObjectName B.ByteString | JSObjectId Int

get_js_object_method :: JSObject -> B.ByteString -> IO JSFunction
get_js_object_method (JSObjectName objname) funcname = do
	i <- BU.unsafeUseAsCStringLen objname $ \(obuf, olen) ->
		BU.unsafeUseAsCStringLen funcname $ \(fbuf, flen) ->
			_get_js_objectname_method obuf olen fbuf flen
	return (JSFunction i)
get_js_object_method (JSObjectId n) funcname = do
	i <- BU.unsafeUseAsCStringLen funcname $ \(fbuf, flen) ->
		_get_js_objectid_method n fbuf flen
	return (JSFunction i)

call_js_function_Void :: JSFunction -> IO ()
call_js_function_Void (JSFunction n) = _call_js_function_void n

call_js_function_ByteString_Void :: JSFunction -> B.ByteString -> IO ()
call_js_function_ByteString_Void (JSFunction n) b =
	BU.unsafeUseAsCStringLen b $ \(buf, len) ->
		_call_js_function_string_void n buf len

call_js_function_ByteString_Object :: JSFunction -> B.ByteString -> IO JSObject
call_js_function_ByteString_Object (JSFunction n) b = do
	i <- BU.unsafeUseAsCStringLen b $ \(buf, len) ->
		_call_js_function_string_object n buf len
	return (JSObjectId i)

call_js_function_Int_Int_Void :: JSFunction -> Int -> Int -> IO ()
call_js_function_Int_Int_Void (JSFunction n) x y =
	_call_js_function_int_int_void n x y
